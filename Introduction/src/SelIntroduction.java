import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SelIntroduction {

	public static void main(String[] args) {
		//Invoking Browser
		//WebDriver methods + class methods
		//Chrome
		System.setProperty("webdriver.chrome.driver","/Users/bdt/Documents/WebDriver/chromedriver");
		WebDriver driver = new ChromeDriver();
		//ChromeDriver driver = new ChromeDriver();
		driver.get("https://rahulshettyacademy.com");
		System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
		//driver.close();//will only close the original window
		driver.quit(); //will close the original window and other associated windows

	}

}
